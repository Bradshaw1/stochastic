

function args(arg, ...)
	if arg then
		return arg.." "..args(...)
	else
		return ""
	end
end

function q(s)
	return '"'..s..'"'
end

function doCMD(command, ...)
	if cmds:len()>0 then
		cmds = cmds.."&& "..command.." "..args(...)
	else
		cmds = command.." "..args(...)
	end
end
function nowCMD()
	os.execute(cmds)
	cmds = ""
end

cmds = ""

local sd = love.filesystem.getSaveDirectory( )

doCMD("cd", q(sd))
doCMD("rsync", "-vr", "radiant", "gaeel@nanoleptic.net:/home/gaeel/")
nowCMD()
