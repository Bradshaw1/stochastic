math.randomseed(os.time())
for i=1,1000000 do
	math.random()
end

function love.load(arg)


	diplodocus = require "diplodocus"
	shad = love.graphics.newShader("shaders/brighten.fs")
	JSON = require("JSON")
	useful = diplodocus.useful
	gstate = require "gamestate"
	game = require "game"
	gstate.registerEvents()
	gstate.switch(game)
end

function love.threaderror(thread, errortext)
    error(errortext) -- Makes sure any errors that happen in the thread are displayed onscreen.
end