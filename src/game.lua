local state = {}

doSync = true
doSave = true
dispRays = true
dispDebug = true
dispComped = true

function placeLine( ... )
	linecount = linecount+1
	love.graphics.line(...)
end

function state:init()
end

function sync()
	if not doSync then
		return
	end
	if (thrd and thrd:isRunning()) then
		print("previous thread still running, waiting for now")
		thrd:wait()
	end
	thrd = love.thread.newThread("sync.lua")
	thrd:start()
end

function line( x1, y1, x2, y2 )
	local l = {
		x1 = x1 or math.random(0,love.graphics.getWidth()),
		y1 = y1 or math.random(0,love.graphics.getHeight()),
		x2 = x2 or math.random(0,love.graphics.getWidth()),
		y2 = y2 or math.random(0,love.graphics.getHeight())
	}
	return l
end

function getABC(l)
	local a = l.y2-l.y1
	local b = l.x1-l.x2
	local c = a*l.x1+b*l.y1
	return a, b, c
end

function lerpline(l1, l2, t)
	return line(
		useful.lerp(l1.x1, l2.x1, t),
		useful.lerp(l1.y1, l2.y1, t),
		useful.lerp(l1.x2, l2.x2, t),
		useful.lerp(l1.y2, l2.y2, t)
	)
end


function state:enter( pre )
	love.graphics.setLineWidth(love.window.getPixelScale())
	love.graphics.setPointSize(love.window.getPixelScale())
	aoff = math.random()*math.pi*2
	adir = math.random()>0.5 and 1 or -1

	targetfps = 30
	raymultiplier = 1
	multiplycorrection = 10

	diffuse = useful.iterate(sinamp, 2, math.random())
	metal = useful.iterate(sinamp, 2, math.random())
	crystal = math.random()>0.75

	firstbounce = math.random()>0.5 and 1 or math.random(1,10)
	bounces = math.random(1,15)
	steppos = {firstbounce}
	for i=2,bounces do
		steppos[i] = 1
	end

	time = 0
	burnt = 0
	rayspersec = 0
	print("A new world!")
	switch = false
	linecount = 0
	canv = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight(), "rgba32f")
	love.graphics.setCanvas(canv)
	love.graphics.clear(0,0,0)

	local wallcount = math.floor(math.random()*15)
	walls = {}
	for i=1,wallcount do
		table.insert(walls,line())
	end
	maxrays = ({1,2,2,4,4,8})[math.random(1,6)]*5000000
	saturate = math.random()>0.5
	maxprec = useful.lerp(0.6,0.9,math.random())
	dodots = math.random()>0.75
	hscatter = math.random()>0.5 and math.sqrt(math.random()) or 0
	gradius = math.random()>0.2 and useful.lerp(0.1,0.4, math.random()) or 1.5
	shape = ({"circle", "disk", "normal"})[math.random(1,3)]
	walled = gradius<1 and math.random()>0.75
	if walled then
		table.insert(walls,line(
			0,
			0,
			love.graphics.getWidth(),
			0
		))
		table.insert(walls,line(
			0,
			love.graphics.getHeight(),
			love.graphics.getWidth(),
			love.graphics.getHeight()
		))
		table.insert(walls,line(
			0,
			0,
			0,
			love.graphics.getHeight()
		))
		table.insert(walls,line(
			love.graphics.getWidth(),
			0,
			love.graphics.getWidth(),
			love.graphics.getHeight()
		))
	end
	ghost = gradius<1 and #walls>0 and math.random()>0.5
	averagebrightness = 0.001
	maxbrightness = 0.001
	weight = 0.5
	comp = 100
	nexttest = 1

	settings = {
		ghost = ghost and "yes" or nil,
		firstbounce = firstbounce,
		bounces = bounces,
		diffuse = math.floor(diffuse*1000)/1000,
		metal = math.floor(metal*1000)/1000,
		saturate = saturate and "yes" or nil,
		maxrays = maxrays,
		maxbright = math.floor(maxprec*1000)/1000,
		wallcount = wallcount,
		crystal = crystal and "yes" or nil,
		walled = walled and "yes" or nil,
		dodots = dodots and "yes" or nil,
		radius = math.floor(gradius*1000)/1000,
		shape = shape,
		huescatter = hscatter>0 and math.floor(hscatter*1000)/1000 or nil
	}
	if gradius > 1 or math.random()>0.5 then
		ptx = love.graphics.getWidth()/2--math.random(0,love.graphics.getWidth())
		pty = love.graphics.getHeight()/2--math.random(0,love.graphics.getHeight())
	else
		local rad = gradius*math.min(love.graphics.getWidth(),love.graphics.getHeight())
		ptx = useful.lerp(rad*1.2,love.graphics.getWidth()-rad*1.2,math.random())
		pty = useful.lerp(rad*1.2,love.graphics.getHeight()-rad*1.2,math.random())
	end
end

function state:leave( next )
end

function doRay(debug)
	local a, d
	function nexto(depth)
		return steppos[depth]
	end
	if shape == "normal" then
		local dx = useful.nrandom()
		local dy = useful.nrandom()
		a = math.atan2(dy,dx)
		d = math.sqrt(dx*dx+dy*dy)*0.25
	elseif shape == "disk" then
		a = math.random()*math.pi*2
		d = math.sqrt(math.random())
	else
		a = math.random()*math.pi*2
		d = 1
	end
	local fa = math.random()*math.pi*2
	local acol = crystal and fa or a
	gr,gg,gb = useful.hsv(((acol*adir+aoff)*360)/(math.pi*2)+math.random()*hscatter*360,saturate and 100 or math.sqrt(d)*100,debug and 100 or 12.5)
	local radius = math.min(love.graphics.getWidth(),love.graphics.getHeight())*gradius
	if (debug) then
		col(255)
		love.graphics.circle("fill", ptx+d*math.cos(a)*radius, pty+d*math.sin(a)*radius, 2.5*love.window.getPixelScale())
	end
	starburst(ptx+d*math.cos(a)*radius, pty+d*math.sin(a)*radius, 0.1, {fa}, nexto, 1, debug)
end


function state:update(dt)
	local lastrays = linecount

	local cfps = 1/dt
	local err = targetfps - cfps
	raymultiplier = raymultiplier-math.max(-1,math.min(1,err))*multiplycorrection*dt

	time = time+dt
	if switch then
		gstate.switch(game)
		switch = false
		return
	end
	nexttest = nexttest-math.min(dt,1/20)*(5*(love.timer.getFPS()/60))
	if nexttest<=0 then
		love.window.setTitle(titlestring())
		local tests = 200000
		local burn = 0
		nexttest = 1
		averagebrightness = 0
		local imdat = canv:newImageData()
		for i=1,tests do
			local r, g, b = imdat:getPixel(math.random(0,imdat:getWidth()-1),math.random(0,imdat:getHeight()-1))
			local m = math.max(r,math.max(g,b))
			local maxpix = m/255
			if maxpix>0.99 then
				burn = burn + 1
			end
			maxbrightness = math.max(maxbrightness, maxpix)
			averagebrightness = averagebrightness+maxpix
		end
		burnt = math.max(burnt,burn*1000/tests)
		averagebrightness = averagebrightness/tests
	end
	local cweight = 1-weight
	comp = useful.lerp(comp, 1/math.max(0.01,useful.lerp(averagebrightness, maxbrightness*1.5, cweight)), dt)

	love.graphics.setCanvas(canv)
	love.graphics.setColor(0,0,0,2)
	love.graphics.setBlendMode("add")
	for i=1,math.floor(100*raymultiplier) do
		doRay()
	end
	if dodots then
		for i=1,math.floor(2500*raymultiplier) do
			local a, d
			if shape == "normal" then
				local dx = useful.nrandom()
				local dy = useful.nrandom()
				a = math.atan2(dy,dx)
				d = math.sqrt(dx*dx+dy*dy)*0.25
			elseif shape == "disk" then
				a = math.random()*math.pi*2
				d = math.sqrt(math.random())
			else
				a = math.random()*math.pi*2
				d = 1
			end
			local acol = a*adir+aoff
			gr,gg,gb = useful.hsv((acol*360)/(math.pi*2)+math.random()*hscatter*360,saturate and 100 or math.sqrt(d)*100,d*50/4)
			local radius = math.min(love.graphics.getWidth(),love.graphics.getHeight())*gradius
			local px = math.random()*love.graphics.getWidth()
			local py = math.random()*love.graphics.getHeight()
			steps = getStepsForPoint(
				px,
				py,
				ptx+d*math.cos(a)*radius,
				pty+d*math.sin(a)*radius
			)
			
			col(8/(1+steps*steps))
			love.graphics.points(px,py)
		end
	end

	rayspersec = useful.lerp(rayspersec, (linecount-lastrays)/dt, dt*10)

	love.graphics.setCanvas()
end

function getStepsForPoint(px, py, sunx, suny)
	hits = raycast(px,py,sunx-px,suny-py, walls, "nosort")
	return #hits
end

function sinamp(x)
	return 0.5-(math.cos(x*math.pi)/2)
end

function starburst(tx, ty, alpha, count, depthfunc, depth, debug)
	local depth = depth or 1
	local c
	local angles
	if type(count)=="table" then
		angles = count
	else
		angles = {}
		for i=1,count do
			table.insert(angles,math.random()*math.pi*2)
		end
	end
	for i=1,#angles do
		local a = angles[i]
		local dx = math.cos(a)
		local dy = math.sin(a)
		local ray = line(tx, ty, tx+dx*4000, ty+dy*4000)
		local hits = raycast(ray.x1,ray.y1,dx,dy,walls)
		while #hits>0 do
			local dx = hits[1].x-tx
			local dy = hits[1].y-ty
			local d = math.sqrt(dx*dx+dy*dy)
			if d<math.pow(10,-10) then
				table.remove(hits,1)
			else
				break
			end
		end
		local alpha = debug and 255 or alpha
		local decay = 0
		if #hits>0 then
			for i,hit in ipairs(hits) do
				if (not ghost) or depth>1 then
					col(alpha)
					placeLine(tx,ty,hit.x,hit.y)
				end
				local cn = depthfunc(depth) or 0
				for _=1,cn do
					local a = math.random()*math.pi*2
					if math.random()>diffuse then
						local ox, oy = reflect(line(hit.x,hit.y, hit.x+hit.nx, hit.y+hit.ny),tx, ty)
						local rx, ry = ox-hit.x, oy-hit.y
						local dr = math.sqrt(rx*rx+ry*ry)
						rx = rx/dr
						ry = ry/dr
						local bx, by = tx-hit.x, ty-hit.y
						local db = math.sqrt(bx*bx+by*by)
						bx = bx/db
						by = by/db
						local nx, ny = useful.lerp(rx,bx,0.5),useful.lerp(ry,by,0.5)
						local sa = math.atan2(ny,nx)+(math.random()*2-1)*math.pi*0.5
						local sx, sy = math.cos(sa), math.sin(sa)
						local fx = useful.lerp(sx, rx, metal)
						local fy = useful.lerp(sy, ry, metal)
						a = math.atan2(fy, fx)
					end
					--]]

					--alpha = alpha*decay
					if cn then
						starburst(hit.x, hit.y, alpha, {a}, depthfunc, depth+1, debug)
					end
				end
				---[[
				return
			end
			if (not ghost) or depth>1 then
				col(alpha)
				placeLine(px,py,ray.x2,ray.y2)
			end
		else
			if (not ghost) or depth>1 then
				col(alpha)
				placeLine(ray.x1,ray.y1,ray.x2,ray.y2)
			end
		end
	end
end

gr = 255
gg = 255
gb = 255

function col(alpha)
	love.graphics.setColor(gr,gg,gb,alpha)
end

function state:draw()
	love.graphics.setBlendMode("alpha")
	love.graphics.setCanvas()
	love.graphics.setColor(255,255,255)
	if dispComped then
		love.graphics.setShader(shad)
		shad:send("brighten", comp)
	end
	love.graphics.draw(canv)
	love.graphics.setShader()


	if time > 300 or (time>120 and (linecount>maxrays or useful.lerp(averagebrightness,maxbrightness*(1+burnt),weight)>maxprec)) then
		if (doSave) then
			local filename = "radiant/radiant-"..os.time()

			love.graphics.setColor(255,255,255)

			love.graphics.setShader(shad)
			shad:send("brighten", comp)
			love.graphics.draw(canv)
			local scrot = love.graphics.newScreenshot(false)
			scrot:encode("png", filename.."-comp.png")


			love.graphics.setShader()
			love.graphics.draw(canv)
			local imdat = love.graphics.newScreenshot(false)
			imdat:encode("png", filename.."-raw.png")



			local json_text = JSON:encode_pretty({
				time = math.floor(time).."s",
				rays = linecount,
				averagebrightness = math.floor(averagebrightness*1000)/1000,
				maxbrightness = math.floor(maxbrightness*1000)/1000,
				settings = settings
			})
			love.filesystem.write(filename.."-settings.json", json_text)
			makeIndex()
			sync()
		end
		switch = true
		prevcanv = canv
		prevcanv:setFilter("linear","linear")
		prevcomp = comp
	end
	if dispRays then
		for i=1,1 do
			doRay(true)
		end
	end
	if dispDebug then
		if prevcanv then
			love.graphics.setColor(128,128,128)
			love.graphics.rectangle("fill",
				love.graphics.getWidth()-prevcanv:getWidth()/5-20-love.window.getPixelScale(),
				20-love.window.getPixelScale(),
				prevcanv:getWidth()/5+love.window.getPixelScale()*2,
				prevcanv:getHeight()/5+love.window.getPixelScale()*2
			)
			love.graphics.setColor(0,0,0)
			love.graphics.rectangle("fill",
				love.graphics.getWidth()-prevcanv:getWidth()/5-20,
				20,
				prevcanv:getWidth()/5,
				prevcanv:getHeight()/5
			)
			love.graphics.setColor(255,255,255)
			love.graphics.setShader(shad)
			shad:send("brighten",prevcomp)
			love.graphics.draw(prevcanv,
				love.graphics.getWidth()-prevcanv:getWidth()/5-20,
				20,
				0,
				1/5,
				1/5
			)
			love.graphics.setShader()
		end


		love.graphics.setColor(0,0,0,128)
		love.graphics.rectangle("fill", 0, 0, 200*love.window.getPixelScale(), love.graphics.getHeight())

		love.graphics.setColor(255,255,255)
		local step = 1
		local stepsize = 15
		local target = useful.lerp(averagebrightness,maxbrightness*(1+burnt),weight)
		love.graphics.scale(love.window.getPixelScale())
		love.graphics.print("rays: "..linecount.."   ("..math.floor(linecount*100/maxrays).."%)",10,step*stepsize); step = step+1
		love.graphics.print("aver: "..(math.floor(averagebrightness*1000)/1000),10,step*stepsize); step = step+1
		love.graphics.print("maxi: "..(math.floor(maxbrightness*1000)/1000),10,step*stepsize); step = step+1
		love.graphics.print("burn: "..(math.floor(burnt*1000)/1000),10,step*stepsize); step = step+1
		love.graphics.print("targ: "..(math.floor(target*1000)/1000).." ("..math.floor(target*100/maxprec).."%)",10,step*stepsize); step = step+1
		love.graphics.print("weighting = "..(math.floor(weight*100)/100),20,step*stepsize); step = step+1
		love.graphics.print("comp: "..(math.floor(comp*10)/10),10,step*stepsize); step = step+1
		love.graphics.print("time: "..math.floor(time).." seconds",10,step*stepsize); step = step+1
		love.graphics.print("rate: "..math.floor(rayspersec),10,step*stepsize); step = step+1
		love.graphics.print("mult: "..math.floor(raymultiplier*100)/100,10,step*stepsize); step = step+1
		love.graphics.print(" f/s: "..love.timer.getFPS(),10,step*stepsize); step = step+1
		for k,v in pairs(settings) do
			if type(v) == "boolean" then
				v = v and "true" or "false"
			end
			love.graphics.print(k..": "..v, 20, step*stepsize)
			step = step+1
		end
		love.graphics.origin()
	end
end

function titlestring()
	local target = useful.lerp(averagebrightness,maxbrightness*(1+burnt),weight)
	local rays = (math.floor(linecount/100000)/10).."mr"
	local raysp = math.floor(linecount*100/maxrays)
	local times = math.floor(time).."s"
	local timesp = math.floor(time*100/300)
	local targ = (math.floor(target*1000)/1000).."lm"
	local targp = math.floor(target*100/maxprec)
	local perc = raysp
	local pstr = "% (rays)"
	if timesp>perc then
		perc = timesp
		pstr = "% (time)"
	end
	if targp>perc then
		perc = targp
		pstr = "% (lume)"
	end
	return "STOCHASTIC RADIANT ALGEBRA - ["..rays.." | "..times.." | "..targ.."] "..perc..pstr
end

function raycast(ptx, pty, dx, dy, cols, dosort)
	local dosort = dosort or "sort"
	local d = math.sqrt(dx*dx+dy*dy)
	local dx = dx/d
	local dy = dy/d
	local ray = line(ptx, pty, ptx+dx*4000, pty+dy*4000)
	local hits = {}
	for i,v in ipairs(cols) do
		local px, py = lineline(ray, v)
		if inbox(px,py,ray) and inbox(px,py,v) then
			local dx = v.x2-v.x1
			local dy = v.y2-v.y1
			local ang = math.atan2(dy,dx)+math.pi/2

			table.insert(hits, {
				x = px,
				y = py,
				nx = math.cos(ang),
				ny = math.sin(ang)
			})
		end
	end
	if (dosort == "sort") then
		table.sort( hits, function(a,b)
			local adx = ray.x1-a.x
			local ady = ray.y1-a.y
			local bdx = ray.x1-b.x
			local bdy = ray.y1-b.y
			local ad2 = adx*adx+ady*ady
			local bd2 = bdx*bdx+bdy*bdy
			return ad2<bd2
		end)
	end
	return hits
end

function orientation(p1, p2, p3)
    local val = (p2.y - p1.y) * (p3.x - p2.x) -
              (p2.x - p1.x) * (p3.y - p2.y)
 
 
    return (val > 0) and 1 or 2
end

--[[
function lineline(x1,y1,x2,y2,x3,y3,x4,y4)
	if (type(x1)=="table") then
		local l1 = x1
		local l2 = y1
		return lineline(l1.x1,l1.y1,l1.x2,l1.y2,l2.x1,l2.y1,l2.x2,l2.y2)
	end
	local px = ((x1*y2 - y1*x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3*x4))
				/ ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
	local py = ((x1*y2 - y1*x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3*x4))
				/ ((x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4))
	return px, py
end
--]]
function lineline(l1,l2,x2,y2,x3,y3,x4,y4)
	if type(x1)=="number" then
		local l1 = line(l1,l2,x2,y2)
		local l2 = line(x3,y3,x4,y4)
		return lineline(l1,l2)
	end
	local a1, b1, c1 = getABC(l1)
	local a2, b2, c2 = getABC(l2)
	return intersect(a1,b1,c1,a2,b2,c2)
end

function intersect(a1,b1,c1,a2,b2,c2)
	local det = a1*b2 - a2*b1
	local px = (b2*c1 - b1*c2)/det
	local py = (a1*c2 - a2*c1)/det
	return px, py
end

function reflect(l, x, y)
	local a1, b1, c1 = getABC(l)
	local a2 = -b1
	local b2 = a1
	local c2 = a2*x+b2*y
	local px, py = intersect(a1,b1,c1,a2,b2,c2)
	return px - (x - px), py - (y - py)
end

function inbox(tx,ty,x1,y1,x2,y2)
	if (type(x1)=="table") then
		local l = x1
		return inbox(tx, ty, l.x1, l.y1, l.x2, l.y2)
	end
	return tx>=math.min(x1,x2) and tx<=math.max(x1,x2) and ty>=math.min(y1,y2) and ty<=math.max(y1,y2)
end

function makeIndex()
	local files = love.filesystem.getDirectoryItems("radiant")
	print("found "..#files)
	local str = "<head><title>STOCHASTIC RADIANT ALGEBRA</title></head><body>"
	for i,v in ipairs(files) do
		print(i,"file:",v)
		str = str .. "<a href="..v..">"..v.."</a><br>"
	end
	str = str .. "</body>"
	love.filesystem.write("radiant/index.html", str)
end


function state:errhand(msg)
end


function state:focus(f)
end


function state:keypressed(key, isRepeat)
	if key=='escape' then
		love.event.push('quit')
	end
	if key=="d" then
		dispDebug = not dispDebug
	end
	if key=="r" then
		dispRays = not dispRays
	end
	if key=="c" then
		dispComped = not dispComped
	end
end


function state:keyreleased(key, isRepeat)
end


function state:mousefocus(f)
end


function state:mousepressed(x, y, btn)
end


function state:mousereleased(x, y, btn)
end


function state:quit()
end


function state:resize(w, h)
end


function state:textinput( t )
end


function state:threaderror(thread, errorstr)
end


function state:visible(v)
end


function state:gamepadaxis(joystick, axis)
end


function state:gamepadpressed(joystick, btn)
end


function state:gamepadreleased(joystick, btn)
end


function state:joystickadded(joystick)
end


function state:joystickaxis(joystick, axis, value)
end


function state:joystickhat(joystick, hat, direction)
end


function state:joystickpressed(joystick, button)
end


function state:joystickreleased(joystick, button)
end


function state:joystickremoved(joystick)
end

return state